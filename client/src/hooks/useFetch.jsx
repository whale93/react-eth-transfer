import { useEffect, useState } from 'react';

const API_KEY = import.meta.env.VITE_GIPHY_API

const useFetch = ({ keyword }) => {
  const [gifUrl, setGifUrl] = useState("");

  const fetchGifs = async () => {
    try {
      const response = await fetch(`https://api.giphy.com/v1/gifs/search?api_key=${API_KEY}&q=${keyword.split(" ").join("")}&limit=1`)
      const { data } = await response.json();

      setGifUrl(data[0]?.images?.downsized_medium?.url)
    } catch (error) {
      const response = await fetch(`https://api.giphy.com/v1/gifs/random?api_key=${API_KEY}&rating=g`)
      const { data } = await response.json();
      setGifUrl(setGifUrl(data[0]?.images?.downsized_medium?.url))
    }
  }
  useEffect(() => {
    if (keyword) fetchGifs();
  }, [keyword]);

  return gifUrl;
}

export default useFetch;