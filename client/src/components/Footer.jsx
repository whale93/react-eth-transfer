import { AiFillGithub } from 'react-icons/ai';

const Footer = () => {
  return (
    <footer className="text-center lg:text-left text-gray-600">
      <div className='h-[1px] w-5/6 bg-gray-400 mx-auto' />
      <div className="flex justify-center items-center w-5/6 mx-auto lg:justify-between p-6 border-b border-gray-300">
        <div className="mr-12 hidden lg:block">
          <span>whale93's react-eth-transfer</span>
        </div>
        <div className="flex justify-center">
          <a href="https://github.com/whale93/react-eth-transfer" className="text-gray-600">
            <AiFillGithub size={25} />
          </a>
        </div>
      </div>

    </footer>
  );
}

export default Footer;