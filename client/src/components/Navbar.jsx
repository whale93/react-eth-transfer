import React, { useContext, useState } from 'react';

import { HiMenu } from 'react-icons/hi';
import { AiOutlineClose } from 'react-icons/ai';
import { TransactionContext } from '../context/TransactionContext';

import logo from '../../images/Whale Logo/whale_base_512px.svg';
import { shortenAddress } from '../utils/shortenAddress';

const NavbarItem = ({ title, classProps }) => {
  return (
    <li className={`mx-4 cursor-pointer ${classProps}`}>
      {title}
    </li>
  )
}

const Navbar = () => {
  const [toggleMenu, setToggleMenu] = useState(false);
  const { connectWallet, currentAccount } = useContext(TransactionContext);

  return (
    <>
      <div className='w-full flex justify-center flex-initial items-center p-4'>
        <img src={logo} alt='logo' className='w-24 cursor-pointer' />
      </div>

      <nav className='w-full flex justify-center'>
        {!currentAccount && (
          <button type="button" onClick={connectWallet} className="flex justify-center w-80 my-5 bg-[#b8dee9] p-3 rounded-full cursor-pointer hover:bg-[#7ab4c4]">
            <p className="text-#1e2c30 text-base font-semibold">Connect to MetaMask</p>
          </button>
        )}

        {currentAccount && (
          <p className="text-center my-5"><b>Wallet Address:</b><br />{shortenAddress(currentAccount)}</p>
        )}

        {/*
                <div className='bg-[#87CEFA] rounded-lg p-3'>
                    <ul className='text-black flex list-none flex-row justify-between items-center flex-initial'>
                        {["Markets", "Exchange", "Wallets", "Docs"].map((item, index) => (
                            <NavbarItem key={item + index} title={item} />
                        ))}
                        <li className='bg-[#2952e3] py-2 px-7 mx-4 rounded-full cursor-pointer hover:bg-[#2546bd]'>
                            Login
                        </li>
                    </ul>
                    <div className='flex relative'>
                        {toggleMenu
                            ? <AiOutlineClose fontSze={28} className='text-black md:hidden cursor-pointer' onClick={() => setToggleMenu(false)}/>
                            : <HiMenu fontSze={28} className='text-black md:hidden cursor-pointer' onClick={() => setToggleMenu(true)}/>
                        }
                        {toggleMenu && (
                        <ul 
                            className='z-10 fixed top-0 -right-2 p-3 w-[70vw] h-screen shadow-2xl md:hidden list-none 
                            flex flex-col justify-start items-end rounded-md blue-glassmorphism text-black animate-slide-in'
                        >
                            <li className='text-xl w-full my-2'>
                            <AiOutlineClose onClick={() => setToggleMenu(false)} />
                            </li>
                            {["Markets", "Exchange", "Wallets", "Docs"].map((item, index) => (
                            <NavbarItem key={item + index} title={item} classProps='my-2 text-lg' />
                            ))}
                        </ul>
                        )}
                    </div>
                </div>
                */}
      </nav>
    </>
  );
}

export default Navbar;