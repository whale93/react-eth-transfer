import React, { useContext, useState } from 'react';

import { TransactionContext } from '../context/TransactionContext';
import { Loader } from './';

const Input = ({ placeholder, name, type, value, handleChange }) => (
  <input
    placeholder={placeholder}
    type={type}
    step="0.0001"
    value={value}
    onChange={(e) => handleChange(e, name)}
    className="my-2 w-full rounded-sm p-2 outline-none bg-transparent text-#1e2c30 border-none text-sm"
  />
);


const Welcome = () => {
  const { formData, sendTransaction, handleChange, isLoading } = useContext(TransactionContext);
  const handleSubmit = (e) => {
    const { addressTo, amount, keyword, message } = formData;
    e.preventDefault(); //Prevents page from reloading when form submitted

    if (!addressTo || !amount || !keyword || !message) return;

    sendTransaction();
  }

  return (
    <div className="mb-auto flex flex-col w-full justify-center items-center">

      <div className="w-full max-w-xs">
        <form className="bg-white shadow-md rounded-lg px-8 pt-6 pb-8 mb-4">
          <div className="mb-4">
            <label className="block text-gray-700 text-sm font-bold mb-2">
              Address To:
            </label>
            <Input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="addressTo" placeholder="0x34C...4z6A" name="addressTo" type="text" handleChange={handleChange} />
          </div>
          <div className="mb-4">
            <label className="block text-gray-700 text-sm font-bold mb-2">
              Amount (ETH):
            </label>
            <Input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="amount" placeholder="0" name="amount" type="number" handleChange={handleChange} />
          </div>
          <div className="mb-4">
            <label className="block text-gray-700 text-sm font-bold mb-2">
              Keyword (GIF):
            </label>
            <Input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="keyword" placeholder="whale" name="keyword" type="text" handleChange={handleChange} />
          </div>
          <div className="mb-6">
            <label className="block text-gray-700 text-sm font-bold mb-2">
              Message:
            </label>
            <Input className="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" id="message" placeholder="Enter a message..." name="message" type="text" handleChange={handleChange} />
          </div>
          <div className="flex justify-center">
            {isLoading
              ? <Loader />
              : (
                <button
                  type="button"
                  onClick={handleSubmit}
                  className="bg-transparent text-lg hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full"
                >
                  Send now
                </button>
              )}
          </div>
        </form>
      </div>
    </div>
  );
}

export default Welcome;