import './App.css'
import { Navbar, Welcome, Footer, Services, Transactions } from "./components"

const App = () => {
  return (
    <div>
      <div className="flex flex-col justify-between gradient-bg-welcome">
        <Navbar />
        <Welcome />
        <Transactions />
        <Footer />
      </div>
      {/*
      <Services />
      */}
    </div>
  )
}

export default App
