require('@nomiclabs/hardhat-waffle')
const ALCHEMY_API_URL = process.env.ALCHEMY_API_URL
const ALCHEMY_API_ACC = process.env.ALCHEMY_API_ACC

module.exports = {
  solidity: '0.8.0',
  networks: {
    ropsten: {
      url: ALCHEMY_API_URL,
      accounts: [ALCHEMY_API_ACC]
    }
  }
}
