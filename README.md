# Crypto Transfer React
[![Netlify Status](https://api.netlify.com/api/v1/badges/2eaeb08d-b143-47c7-83d6-50e1ec47e08d/deploy-status)](https://react-eth-transfer.netlify.app/)
<br><br>Tutorial Source: https://www.youtube.com/watch?v=Wn_Kb3MR_cU

The React app is able to connect to your MetaMask wallet and interact with a smart contract to send ethereum and store a keyword and message.

Run the application with `'npm run dev'` in the "client" folder.

Add giphy api key by creating a .env file in client/src and adding the lines
`'VITE_GIPHY_API=YOUR_API_KEY'`

Add alchemy key by creating a .env file in client/smart_contract and adding the line 
`'ALCHEMY_API_URL=YOUR_ALCHEMY_API_URL'`<br>`'ALCHEMY_API_ACC=YOUR_ALCHEMY_API_ACC'`
